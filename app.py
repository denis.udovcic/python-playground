from flask import Flask, request
from flask_restplus import Api, Resource
from db import session_factory
from datetime import date
from person import Person
from serializer import serializePerson
from flask_cors import CORS

app = Flask(__name__)
api = Api(app)

CORS(app)

todos = {}
created = False

def create_people():
    session = session_factory()
    bruno = Person("Bruno Krebs", date(1984, 10, 20), 182, 84.5)
    john = Person("John Doe", date(1990, 5, 17), 173, 90)
    session.add(bruno)
    session.add(john)
    session.commit()
    session.close()

def createNewPerson(data):
    session = session_factory()
    newPerson = Person(data['name'], data['date_of_birth'], data['height'], data['weight'])
    session.add(newPerson)
    session.commit()
    session.close()

def get_people():
    session = session_factory()
    people_query = session.query(Person)
    session.close()
    return people_query.all()

def search_people(query):
    session = session_factory()
    people_query = session.query(Person).filter(Person.name.ilike("%{}%".format(query)))
    session.close()
    return people_query.all()

def deletePersonById(id):
    try:
        session = session_factory()
        session.query(Person).filter(Person.id==id).delete()
        session.commit()
        session.close()
        return True
    except Exception as e:
        print('there was an error')
        print(e)
        return False

def dictFromList(list):
    newDict = {}
    # for key, value in list:
        # newDict[key] = value
    return newDict

@api.route('/createstuff')
class CreateStuff(Resource):
    def get(self):
        global created
        if not created:
            create_people()
            created = True
            return 'Just Created'
        return 'Already Created'

@api.route('/getpeople')
class GetStuff(Resource):
    def get(self):
        people = get_people()
        peopleList = []
        for person in people:
            p = serializePerson(person)
            print(p)
            peopleList.append(p)
        # return 'printed them'
        return peopleList

@api.route('/addUser')
class AddUser(Resource):
    def put(self):
        data = request.json or {}
        print('prointing data', data)

        if not all(key in data for key in ['name', 'date_of_birth', 'height', 'weight']):
            return 'bad request', 400

        try:
            createNewPerson(data)
        except Exception as e:
            print('error :()', e)
            return 'failed'
        return 'success', 200

@api.route('/search/<string:query>')
class SearchPeople(Resource):
    def get(self, query):
        people_results =  search_people(query)
        return [serializePerson(p) for p in people_results]

@api.route('/<string:todo_id>')
class HelloWorld(Resource):
    def get(self, todo_id):
        if todo_id not in todos.keys():
            return { 'error': 'key not present' }
        return { todo_id: todos[todo_id] }

    def put(self, todo_id):
        todos[todo_id] = request.form['data']
        print(request.form)
        return { todo_id: todos[todo_id] }

    def delete(self, todo_id):
        print('deleting %s' % todo_id)
        deleted = deletePersonById(todo_id)
        if not deleted:
            return 'not deleted'
        return 'deleted'
        

if __name__ == '__main__':
    app.run(debug=True)