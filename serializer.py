def serializePerson(person):
    return {
        'id': person.id,
        'name': person.name,
        'date_of_birth': str(person.date_of_birth),
        'height': int(person.height),
        'weight': int(person.weight),
    }